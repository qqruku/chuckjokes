package com.galashev.myapplication.di

import com.galashev.myapplication.AppDelegate
import io.realm.Realm
import toothpick.config.Module

class AppModule(appDelegate: AppDelegate) : Module() {

    private var mApp: AppDelegate? = null

    init {
        mApp = appDelegate
        bind(AppDelegate::class.java).toInstance(mApp)
        bind(Realm::class.java).toInstance(getRealm())
    }

    fun provideApp(): AppDelegate? = mApp

    private fun getRealm(): Realm? = Realm.getDefaultInstance()

}