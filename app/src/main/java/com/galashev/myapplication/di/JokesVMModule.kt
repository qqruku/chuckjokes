package com.galashev.myapplication.di

import com.galashev.myapplication.ui.jokes.JokesViewModel
import com.galashev.myapplication.ui.jokes.SavedJokesViewModel
import toothpick.config.Module

class JokesVMModule : Module() {
    init {
        bind(JokesViewModel::class.java).to(JokesViewModel::class.java).singletonInScope()
        bind(SavedJokesViewModel::class.java).to(SavedJokesViewModel::class.java).singletonInScope()
    }
}