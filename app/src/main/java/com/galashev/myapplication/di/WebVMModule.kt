package com.galashev.myapplication.di

import com.galashev.myapplication.ui.web.WebFragmentViewModel
import toothpick.config.Module

class WebVMModule : Module() {
    init {
        bind(WebFragmentViewModel::class.java).toInstance(WebFragmentViewModel())
    }
}