package com.galashev.myapplication.di

import com.galashev.myapplication.BuildConfig
import com.galashev.myapplication.ChuckNorrisApi
import com.google.gson.Gson
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import toothpick.config.Module

class NetworkModule : Module() {

    private val mGson = Gson()
    private val mOkHttpClient: OkHttpClient = getClient()
    private val mRetrofit: Retrofit = getRetrofit()

    init {
        bind(Gson::class.java).toInstance(mGson)
        bind(OkHttpClient::class.java).toInstance(mOkHttpClient)
        bind(Retrofit::class.java).toInstance(mRetrofit)
        bind(ChuckNorrisApi::class.java).toInstance(getApiService())
    }

    private fun getClient(): OkHttpClient {
        val builder = OkHttpClient().newBuilder()
        if (!BuildConfig.BUILD_TYPE.contains("release")) {
            builder.addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
        }
        return builder.build()
    }

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.API_URL)
            .client(mOkHttpClient)
            .addConverterFactory(GsonConverterFactory.create(mGson))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
    }

    private fun getApiService(): ChuckNorrisApi {
        return mRetrofit.create(ChuckNorrisApi::class.java)
    }

}