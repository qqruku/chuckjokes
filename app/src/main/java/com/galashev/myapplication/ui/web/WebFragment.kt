package com.galashev.myapplication.ui.web

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import androidx.fragment.app.Fragment
import com.galashev.myapplication.AppDelegate
import com.galashev.myapplication.databinding.FrWebBinding
import toothpick.Toothpick
import javax.inject.Inject

class WebFragment : Fragment() {

    @Inject
    @JvmField
    var webViewModel: WebFragmentViewModel? = null
    private lateinit var webView: WebView

    companion object {
        fun newInstance(): WebFragment {
            return WebFragment()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        Toothpick.inject(this, AppDelegate::appScope.get(AppDelegate()))
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FrWebBinding.inflate(inflater, container, false)
        binding.vm = webViewModel
        binding.lifecycleOwner = this
        webView = binding.webView
        webView.loadUrl(webViewModel!!.url)
        return binding.root
    }

}