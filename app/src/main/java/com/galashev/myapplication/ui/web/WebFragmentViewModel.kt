package com.galashev.myapplication.ui.web

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class WebFragmentViewModel() : ViewModel() {

    val url: String = "http://www.icndb.com/api/"
    val progressBarVisibility : MutableLiveData<Boolean> = MutableLiveData()



}