package com.galashev.myapplication.ui.jokes

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.galashev.myapplication.AppDelegate
import com.galashev.myapplication.ChuckNorrisApi
import com.galashev.myapplication.model.Joke
import io.realm.Realm
import io.realm.RealmResults
import toothpick.Toothpick
import javax.inject.Inject

class SavedJokesViewModel : ViewModel() {

    @Inject
    lateinit var api: ChuckNorrisApi

    @Inject
    lateinit var realm: Realm

    val listOfJokes: MutableLiveData<List<Joke>> = MutableLiveData()

    init {
        Toothpick.inject(this, AppDelegate::appScope.get(AppDelegate()))
        val realmJokes: RealmResults<Joke> = realm.where(Joke::class.java)!!.findAll()
        val jokes: List<Joke>? = realm.copyFromRealm(realmJokes)
        listOfJokes.value = jokes
    }

    override fun onCleared() {
        realm.close()
    }

}