package com.galashev.myapplication.ui.jokes

import androidx.recyclerview.widget.RecyclerView
import com.galashev.myapplication.databinding.JokeBinding
import com.galashev.myapplication.model.Joke

class JokesHolder(private val binding: JokeBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: Joke) {

        binding.joke = JokesListItemViewModel(item)
        binding.executePendingBindings()
    }

}
