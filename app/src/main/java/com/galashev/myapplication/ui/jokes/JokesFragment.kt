package com.galashev.myapplication.ui.jokes

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.galashev.myapplication.AppDelegate
import com.galashev.myapplication.databinding.FrJokesBinding
import toothpick.Toothpick
import javax.inject.Inject

class JokesFragment : Fragment() {


    @Inject
    @JvmField
    var jokesViewModel: JokesViewModel? = null

    companion object {
        fun newInstance(): JokesFragment {
            return JokesFragment()
        }
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        Toothpick.inject(this, AppDelegate::appScope.get(AppDelegate())/*Toothpick.openScope(AppDelegate::class.java)*/)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val binding = FrJokesBinding.inflate(inflater, container, false)
        binding.vm = jokesViewModel
        binding.lifecycleOwner = this
        return binding.root
    }
}