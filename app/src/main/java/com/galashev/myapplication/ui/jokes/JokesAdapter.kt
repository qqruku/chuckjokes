package com.galashev.myapplication.ui.jokes

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.galashev.myapplication.databinding.JokeBinding
import com.galashev.myapplication.model.Joke

class JokesAdapter(private val jokes: List<Joke>) : RecyclerView.Adapter<JokesHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokesHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = JokeBinding.inflate(inflater, parent, false)
        return JokesHolder(binding)
    }

    override fun onBindViewHolder(holder: JokesHolder, position: Int) {
        holder.bind(jokes[position])
    }

    override fun getItemCount(): Int {
        return jokes.size
    }
}