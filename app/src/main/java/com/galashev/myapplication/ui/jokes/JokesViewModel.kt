package com.galashev.myapplication.ui.jokes

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.galashev.myapplication.AppDelegate
import com.galashev.myapplication.ChuckNorrisApi
import com.galashev.myapplication.model.Joke
import com.galashev.myapplication.model.JokeResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import toothpick.Toothpick
import javax.inject.Inject

class JokesViewModel : ViewModel() {

    @Inject
    lateinit var api: ChuckNorrisApi

    @Inject
    lateinit var realm: Realm

    private var disposable: Disposable? = null
    val countOfJokes: MutableLiveData<String> = MutableLiveData()
    val listOfJokes: MutableLiveData<List<Joke>> = MutableLiveData()

    init{
        Toothpick.inject(this, AppDelegate::appScope.get(AppDelegate()))
        listOfJokes.value = ArrayList()
    }

    fun reloadJokes() {
        disposable = api.getJokes(countOfJokes.value ?: "0")
            .map(JokeResponse::jokes)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { response ->
                    listOfJokes.value = response
                    insertJokes(response)
                },
                { throwable ->
                    Log.d("TAG", "reloadJokes: error")
                })
    }

    fun insertJokes(list: List<Joke>?) {
        if (list != null) {
            realm.executeTransactionAsync { realm -> for (j in list) realm.insertOrUpdate(j) }
        }
        val results: RealmResults<Joke> = realm.where(Joke::class.java)!!.findAll()
        val jokes: List<Joke>? = realm.copyFromRealm(results)
    }

    override fun onCleared() {
        disposable = null
        realm.close()
    }

}