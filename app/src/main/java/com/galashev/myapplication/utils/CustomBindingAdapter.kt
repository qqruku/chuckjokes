package com.galashev.myapplication.utils

import android.annotation.SuppressLint
import android.webkit.*
import androidx.databinding.BindingAdapter
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.galashev.myapplication.model.Joke
import com.galashev.myapplication.ui.jokes.JokesAdapter


@BindingAdapter("bind:data")
fun configureRecyclerView(
    recyclerView: RecyclerView,
    jokes: MutableLiveData<List<Joke>>
) {
    val adapter = JokesAdapter(jokes.value!!)
    recyclerView.layoutManager = LinearLayoutManager(recyclerView.context)
    recyclerView.adapter = adapter
}

@SuppressLint("SetJavaScriptEnabled")
@BindingAdapter("bind:configure")
fun configureWebView(
    browser: WebView,
    progressBarVisibility: MutableLiveData<Boolean>
) {
    with(browser){
        webChromeClient = object : WebChromeClient(){
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                when (newProgress){
                    100 -> progressBarVisibility.value = false
                    else -> progressBarVisibility.value = true
                }
            }
        }
        with(settings){
            javaScriptEnabled = true
            useWideViewPort = true
            loadWithOverviewMode = true
        }
    }
}