package com.galashev.myapplication

import android.app.Application
import com.galashev.myapplication.di.AppModule
import com.galashev.myapplication.di.JokesVMModule
import com.galashev.myapplication.di.NetworkModule
import com.galashev.myapplication.di.WebVMModule
import io.realm.Realm
import toothpick.Scope
import toothpick.Toothpick
import toothpick.configuration.Configuration
import toothpick.registries.FactoryRegistryLocator
import toothpick.registries.MemberInjectorRegistryLocator
import toothpick.smoothie.module.SmoothieApplicationModule

class AppDelegate : Application() {

    val appScope: Scope = Toothpick.openScope(AppDelegate::class.java)

    override fun onCreate() {
        super.onCreate()

        Realm.init(this)

        Toothpick.setConfiguration(Configuration.forProduction().disableReflection())
        MemberInjectorRegistryLocator.setRootRegistry(MemberInjectorRegistry())
        FactoryRegistryLocator.setRootRegistry(FactoryRegistry())

        appScope.installModules(
            SmoothieApplicationModule(this),
            NetworkModule(),
            AppModule(this),
            JokesVMModule(),
            WebVMModule()
        )
    }

}