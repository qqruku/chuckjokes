package com.galashev.myapplication.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class JokeResponse(
    @SerializedName("value")
    var jokes: List<Joke>
    ) : Serializable