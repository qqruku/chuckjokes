package com.galashev.myapplication.model

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.io.Serializable

open class Joke(
    @SerializedName("id")
    @PrimaryKey
    var id: Int = 0,

    @SerializedName("joke")
    var jokeText: String = "",

    @SerializedName("published_on")
    var publishedOn: Long = 0
) : Serializable, RealmObject()
