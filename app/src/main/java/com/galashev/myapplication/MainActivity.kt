package com.galashev.myapplication

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.galashev.myapplication.ui.jokes.JokesFragment
import com.galashev.myapplication.ui.jokes.SavedJokesFragment
import com.galashev.myapplication.ui.web.WebFragment
import com.google.android.material.bottomnavigation.BottomNavigationView


class MainActivity : AppCompatActivity() {

    private lateinit var bottomNavView: BottomNavigationView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavView = findViewById(R.id.bottomNavView)

        bottomNavView.setOnNavigationItemSelectedListener{
                item ->
            when(item.itemId) {
                R.id.jokes -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, JokesFragment.newInstance())
                        .commit()
                    true
                }

                R.id.savedJokes -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, SavedJokesFragment.newInstance())
                        .commit()
                    true
                }

                R.id.web -> {
                    supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, WebFragment.newInstance())
                        .commit()
                    true
                }
                else -> false
            }

        }
        if (savedInstanceState == null){
            supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, JokesFragment.newInstance())
                .commit()
            bottomNavView.selectedItemId = R.id.jokes
        }
    }
}