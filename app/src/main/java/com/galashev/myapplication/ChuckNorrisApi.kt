package com.galashev.myapplication

import com.galashev.myapplication.model.JokeResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path

interface ChuckNorrisApi {

    @GET("jokes/random/{count_of_jokes}")
    fun getJokes(@Path("count_of_jokes") jokesCount: String): Single<JokeResponse>

}